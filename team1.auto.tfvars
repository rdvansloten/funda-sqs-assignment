sqs_team1 = {
  test_devops_makelaars = {
    sqs_messages_warning  = 11
    sqs_messages_critical = 26
    warning_notify        = "@team1 @team2"
    critical_notify       = "@team3"
  }
  test_devops_makelaars_errors = {
    sqs_messages_warning  = 0.1
    sqs_messages_critical = 1
    warning_notify        = ""
    critical_notify       = "@team1 @team2 @team3"
  }
}