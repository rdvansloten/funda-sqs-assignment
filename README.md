# Funda SQS Assignment


## Requirements
- Install [Terraform 0.13+](https://releases.hashicorp.com/terraform/0.13.0-beta3/)

## Variables

### Mandatory
```YAML
-
```

### Optional
```YAML
delay_seconds:             [number]
max_message_size:          [number]
message_retention_seconds: [number]
tags:                      [object]
```

## Usage

You can run the module using the following example.

```HCL
# Amazon
provider "aws" {
  region     = "us-west-2"
}

# Woof
provider "datadog" {
  api_key = "${var.datadog_api_key}"
  app_key = "${var.datadog_app_key}"
}

module "sqs_team1" {
  source                        = "./modules/sqs"
  for_each                      = var.sqs_team1
  sqs_name                      = each.key
  sqs_delay_seconds             = each.value.sqs_delay_seconds
  sqs_max_message_size          = each.value.sqs_max_message_size
  sqs_message_retention_seconds = each.value.sqs_message_retention_seconds
  sqs_messages_limit            = each.value.sqs_messages_limit
}
```
