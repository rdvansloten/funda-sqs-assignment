locals {
  common_tags = {
    environment = "assignment"
    owner       = "rdvansloten"
  }
}

resource "aws_sqs_queue" "sqs_queue" {
  name                      = var.sqs_name
  delay_seconds             = var.sqs_delay_seconds
  max_message_size          = var.sqs_max_message_size
  message_retention_seconds = var.sqs_message_retention_seconds
  tags                      = local.common_tags
}

resource "datadog_monitor" "sqs_alert" {
  name               = "SQS Alert - ${var.sqs_name}"
  type               = "metric alert"
  message            = "There are {{value}} messages in the queue. {{#is_warning}}Notified: ${var.warning_notify}{{/is_warning}} {{#is_alert}}Notified: ${var.critical_notify}{{/is_alert}} {{#is_recovery}}Notified: ${var.warning_notify} ${var.critical_notify}{{/is_recovery}}"
  escalation_message = "Wake up @rdvansloten"

  query = "avg(last_5m):max:aws.sqs.approximate_number_of_messages_visible{environment:assignment,owner:rdvansloten,queuename:${var.sqs_name}} by {queuename} >= ${var.sqs_messages_critical}"

  thresholds = {
    ok                = 0
    warning           = var.sqs_messages_warning
    warning_recovery  = 0
    critical          = var.sqs_messages_critical
    critical_recovery = 0
  }

  notify_no_data    = false
  renotify_interval = 60
  notify_audit      = false
  timeout_h         = 60
  include_tags      = true

  lifecycle {
    ignore_changes = [silenced]
  }
}