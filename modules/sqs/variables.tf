variable "sqs_name" {
  description = "Desired name of your queue."
  type        = string
}

variable "sqs_delay_seconds" {
  description = "The time in seconds that the delivery of all messages in the queue will be delayed."
  type        = string
  default     = "0"
}

variable "sqs_max_message_size" {
  description = "The limit of how many bytes a message can contain before Amazon SQS rejects it."
  type        = string
  default     = "262144"
}

variable "sqs_message_retention_seconds" {
  description = "The number of seconds Amazon SQS retains a message."
  type        = string
  default     = "345600"
}

variable "sqs_messages_warning" {
  description = "The amount of messages that generate a warning message."
  type        = string
}

variable "sqs_messages_critical" {
  description = "The amount of messages that generate a critical message."
  type        = string
}

variable "warning_notify" {
  description = "The teams to notify when there's a warning message."
  type        = string
}

variable "critical_notify" {
  description = "The teams to notify when there's a critical message."
  type        = string
}
