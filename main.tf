variable sqs_team1 {
  type = map(any)
}

variable sqs_team2 {
  type = map(any)
}

variable sqs_team3 {
  type = map(any)
}

module "sqs_team1" {
  source                = "./modules/sqs"
  for_each              = var.sqs_team1
  sqs_name              = each.key
  sqs_messages_warning  = each.value.sqs_messages_warning
  sqs_messages_critical = each.value.sqs_messages_critical
  warning_notify        = each.value.warning_notify
  critical_notify       = each.value.critical_notify
}

module "sqs_team2" {
  source                = "./modules/sqs"
  for_each              = var.sqs_team2
  sqs_name              = each.key
  sqs_messages_warning  = each.value.sqs_messages_warning
  sqs_messages_critical = each.value.sqs_messages_critical
  warning_notify        = each.value.warning_notify
  critical_notify       = each.value.critical_notify
}

module "sqs_team3" {
  source                = "./modules/sqs"
  for_each              = var.sqs_team3
  sqs_name              = each.key
  sqs_messages_warning  = each.value.sqs_messages_warning
  sqs_messages_critical = each.value.sqs_messages_critical
  warning_notify        = each.value.warning_notify
  critical_notify       = each.value.critical_notify
}