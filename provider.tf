# Terraform
terraform {
  backend "s3" {
    bucket = "funda-sqs-assignment"
    key    = "global/s3/terraform.tfstate"
    region = "eu-central-1"
  }
}

# Amazon
provider "aws" {

}

# Woof
provider "datadog" {

}