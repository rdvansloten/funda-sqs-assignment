sqs_team3 = {
  test_devops_stats_phone_clicks = {
    sqs_messages_warning  = 11
    sqs_messages_critical = 26
    warning_notify        = ""
    critical_notify       = "@team3"
  }
  test_devops_stats_phone_clicks_errors = {
    sqs_messages_warning  = 0.1
    sqs_messages_critical = 1
    warning_notify        = ""
    critical_notify       = "@team1 @team2 @team3"
  }
  test_devops_stats_facebook_clicks = {
    sqs_messages_warning  = 11
    sqs_messages_critical = 26
    warning_notify        = ""
    critical_notify       = "@team3"
  }
  test_devops_stats_facebook_clicks_errors = {
    sqs_messages_warning  = 0.1
    sqs_messages_critical = 1
    warning_notify        = ""
    critical_notify       = "@team1 @team2 @team3"
  }
  test_devops_rudy_errors = {
    sqs_messages_warning  = 0.1
    sqs_messages_critical = 1
    warning_notify        = ""
    critical_notify       = "@rdvansloten"
  }
}